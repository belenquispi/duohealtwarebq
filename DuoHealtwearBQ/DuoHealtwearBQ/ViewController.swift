//
//  ViewController.swift
//  DuoHealtwearBQ
//
//  Created by Belen Quispi on 20/6/17.
//  Copyright © 2017 Belen Quispi. All rights reserved.
//

import UIKit


class ViewController: UIViewController {

    
    
    @IBOutlet weak var labelLabel: UILabel!
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var activeAccountButton: UIButton!
    
    var rojo, verde, azul, alpha : CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view, typically from a nib.
    
        // Imagen redonda
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        rojo = 139/255.0
        verde = 170/255.0
        azul = 198/255.0
        alpha = 1
        let myColor : UIColor = UIColor.init(red: rojo, green: verde, blue: azul, alpha: alpha)
        logInButton.layer.backgroundColor = myColor.cgColor
        activeAccountButton.layer.borderColor = myColor.cgColor;
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: rojo, green: verde, blue: azul, alpha: alpha)
        self.navigationController?.navigationBar.tintColor = UIColor.blue
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.black]
        self.view.backgroundColor = myColor
        
        self.navigationItem.title =  " "
        logInButton.layer.cornerRadius = 15
        activeAccountButton.layer.borderWidth = 5
        activeAccountButton.layer.cornerRadius = 15
        
        activeAccountButton.setTitleColor(myColor, for: .normal)
        labelLabel.textColor = myColor
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

