//
//  ViewController1.swift
//  DuoHealtwearBQ
//
//  Created by Belén on 3/7/17.
//  Copyright © 2017 Belen Quispi. All rights reserved.
//

import UIKit

class ViewController1: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var passTextField: UITextField!
    
    var rojo, verde, azul, alpha : CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title =  " "
        nameTextField.layer.cornerRadius = 15
        passTextField.layer.cornerRadius = 15
        
        rojo = 139/255.0
        verde = 170/255.0
        azul = 198/255.0
        alpha = 1
        let myColor : UIColor = UIColor.init(red: rojo, green: verde, blue: azul, alpha: alpha)
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: rojo, green: verde, blue: azul, alpha: alpha)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.black]
        self.view.backgroundColor = myColor
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
