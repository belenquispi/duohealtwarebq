//
//  CodeViewController.swift
//  DuoHealtwearBQ
//
//  Created by Belen Quispi on 28/6/17.
//  Copyright © 2017 Belen Quispi. All rights reserved.
//

import UIKit
import SmileLock

class CodeViewController: UIViewController {

    @IBOutlet weak var passwordStackView: UIStackView!
    
    var passwordContainerView: PasswordContainerView!
    let kPasswordDigit = 4
    
      var rojo, verde, azul, alpha : CGFloat!
    override func viewDidLoad() {
        super.viewDidLoad()
     
        //create PasswordContainerView
        passwordContainerView = PasswordContainerView.create(in: passwordStackView, digit: kPasswordDigit)
        passwordContainerView.delegate = self as! PasswordInputCompleteProtocol
        passwordContainerView.deleteButtonLocalizedTitle =  "smilelock_delete"
        
        //customize password UI
        rojo = 150/255.0
        verde = 190/255.0
        azul = 198/255.0
        alpha = 1
        let myColor : UIColor = UIColor.init(red: rojo, green: verde, blue: azul, alpha: alpha)

       passwordContainerView.tintColor = UIColor.black
       passwordContainerView.highlightedColor = myColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title =  " "
        rojo = 139/255.0
        verde = 170/255.0
        azul = 198/255.0
        alpha = 1
        let myColor : UIColor = UIColor.init(red: rojo, green: verde, blue: azul, alpha: alpha)
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: rojo, green: verde, blue: azul, alpha: alpha)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.black]
        self.view.backgroundColor = myColor
        }
    

  
}



extension CodeViewController: PasswordInputCompleteProtocol {
    func passwordInputComplete(_ passwordContainerView: PasswordContainerView, input: String) {
        if validation(input) {
            validationSuccess()
        } else {
            validationFail()
        }
        
    }
    
    func touchAuthenticationComplete(_ passwordContainerView: PasswordContainerView, success: Bool, error: Error?) {
        if success {
            self.validationSuccess()
        } else {
            passwordContainerView.clearInput()
        }
    }
}

private extension CodeViewController {
    func validation(_ input: String) -> Bool {
        return input == "1234"
    }
    
    func validationSuccess() {
        print("*️⃣ success!")
        dismiss(animated: true, completion: nil)
        performSegue(withIdentifier: "push", sender: self)
        
        
    }
    
    func validationFail() {
        print("*️⃣ failure!")
        passwordContainerView.wrongPassword()
    }
    
    
}

